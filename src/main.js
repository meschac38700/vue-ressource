import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'


Vue.config.productionTip = false
Vue.use(VueResource)


Vue.http.options.root = "http://jsonplaceholder.typicode.com";
// add header
Vue.http.headers.common['Authorization'] = "Basic YXBpOnBhc3N3b3Jk";

Vue.http.interceptors.push( (request, next)=>{
  next((response)=>{
    // define after callback
    if( request.after )
    {
      // this = the current Vue instance
      request.after.call(this, response);
    }
  });
})

// Vue.http.interceptors.push((request, next)=>{
//   // modify request
//   request.method = 'POST';
//   // continue to next interceptor
//   next((response)=> {
//     // modify response
//     response.body = 'New Body';
//   });
// });

new Vue({
  render: h => h(App),
  // http: {
  //   root: 'http://jsonplaceholder.typicode.com',
  //   headers:{
  //     Authorization: 'Basic YXBpOnBhc3N3b3Jk',
  //   }
  // }
}).$mount('#app')
